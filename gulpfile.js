//  ==========================================================  GULP
var gulp = require('gulp');

//  ==========================================================  PLUGINS
var gulpsass = require('gulp-sass'),
    sass = require('gulp-ruby-sass'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    useref = require('gulp-useref'),
    clean = require('gulp-rimraf'),
    useref = require('gulp-useref-plus'),
    gulpIf = require('gulp-if'),
    sourcemaps = require('gulp-sourcemaps'),
    cssbeautify = require('gulp-cssbeautify');

//  =========================================================   For browser sync (nb check : http://localhost:3001 for Browsers sync UI)




//  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  DEVELOPMENT TASK WORKFLOW
//  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



//  ==============================================================================  COMPILE SASS APP
var SRC_SCSS = 'app/stylesheets/**/*.scss';
var SRC_CSS = 'app/stylesheets';

    gulp.task('sass', function () {
        return sass(SRC_SCSS, {
                noCache: true,
                sourcemap: true,
                //sourcemapPath: 'app/stylesheets',
                style: 'compact'
        })
        .on('error', function(err){console.log(err.message);})
        .pipe(sourcemaps.write('.'))

     // return gulp
        // Find all `.scss` files from the `stylesheets/` folder
       // .src(SRC_SCSS)

      // init source map
       // .pipe(sourcemaps.init())
        // Run Sass on those files
        //.pipe(sass()
       //.on('error', sass.logError))
        // write sources map
      // .pipe(sourcemaps.write('./'))
        //beautify css
       // .pipe(cssbeautify({
       //     indent: '  ',
        //    openbrace: 'separate-line',
       //     autosemicolon: true
       // }))
        // Write the resulting CSS in the output folder
       .pipe(gulp.dest(SRC_CSS))
        //rename the scss
        //.pipe(rename('styles.min.css'))
        //minify the css
        //.pipe(minifyCss())
        //move the file
        //.pipe(gulp.dest('dist/css'))
        //browser sync
      .pipe(browserSync.reload({stream:true}));
    });

//  ==============================================================================  COMPILE SASS FONTAWESOME (FA)
var SRC_SCSS_FA = 'app/libs/fontawesome/scss/*.scss';
var SRC_CSS_FA = 'app/libs/fontawesome/css';

    gulp.task('sass_fa', function () {
        return sass(SRC_SCSS_FA, {
                noCache: true,
                sourcemap: true,
                //sourcemapPath: 'app/stylesheets',
                style: 'compact'
        })
        .on('error', function(err){console.log(err.message);})
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(SRC_CSS_FA))

    });

//  ============================================================================  BROWSER SYNC ALL HTML
var SRC_HTML = "app/**/*.html";

    gulp.task('html', function () {
        return gulp.src(SRC_HTML)
            .pipe(browserSync.reload({
              stream: true
            }));
    });


//  ==============================================================================  BROWSER SYNC ALL PHP
var SRC_PHP = "app/**/*.php";

    gulp.task('php', function () {
        return gulp.src(SRC_PHP)
            .pipe(browserSync.reload({
              stream: true
            }));
    });

//  ===============================================================================  BROWSER SYNCHRONISATION PATHs
    gulp.task('browserSync', function() {
      browserSync({
         proxy: "http://localhost/formations/02_projet_web_fin_2016/fed-2/app"
      })
    })

//  =============================================================================== LINK FILES FROM SRC TO APP

/*src bower compoent path*/
var BOWER_COMPONENT_PATH = 'src/bower_components/'
var SRC_BOOTSTRAP_CSS = BOWER_COMPONENT_PATH+'bootstrap/dist/css/bootstrap.css';
/*var SRC_BOOTSTRAP_FLEX_CSS =BOWER_COMPONENT_PATH+'bootstrap/dist/css/bootstrap-flex.css';*/  /*fichier bootstrap-flex.css ds dossier styleshhets*/
var SRC_FA_CSS = BOWER_COMPONENT_PATH+'font-awesome/css/font-awesome.css';
var SRC_SWIPER_CSS = BOWER_COMPONENT_PATH+'swiper/dist/css/swiper.css';
var SRC_WHHG_CSS = BOWER_COMPONENT_PATH+'webhostinghub-glyphs/css/webhostinghub-glyphs.css';



var SRC_BOOTSTRAP_JS = BOWER_COMPONENT_PATH+'bootstrap/dist/js/bootstrap.js';
var SRC_JQUERY_JS = BOWER_COMPONENT_PATH+'jquery/dist/jquery.js';
var SRC_MIXITUP_JS = BOWER_COMPONENT_PATH+'mixitup/dist/mixitup.js';
var SRC_SWIPER_JS = BOWER_COMPONENT_PATH+'swiper/dist/js/swiper.js';


/*=========================================================================    CSS ========================*/
   gulp.task('bootstrap_css', function () {
      return gulp
        // Find all `.css` files from the `stylesheets/` folder
        .src(SRC_BOOTSTRAP_CSS)
        //rename the scss
        .pipe(rename({
                    suffix: ".min",
                }))
        //minify the css
        .pipe(minifyCss())
        //move the file // Write the resulting CSS in the output folder
        .pipe(gulp.dest('app/libs/bootstrap/css'))
        //browser sync
        //.pipe(browserSync.reload({stream:true}));

    });



    gulp.task('fa_css', function () {
      return gulp
        // Find all `.css` files from the `stylesheets/` folder
        .src(SRC_FA_CSS)
        //rename the scss
        .pipe(rename({
                    suffix: ".min",
                }))
        //minify the css
        .pipe(minifyCss())
        //move the file // Write the resulting CSS in the output folder
        .pipe(gulp.dest('app/libs/font-awesome/css'))
        //browser sync
        //.pipe(browserSync.reload({stream:true}));

    });

    gulp.task('swiper_css', function () {
      return gulp
        // Find all `.css` files from the `stylesheets/` folder
        .src(SRC_SWIPER_CSS)
        //rename the scss
        .pipe(rename({
                    suffix: ".min",
                }))
        //minify the css
        .pipe(minifyCss())
        //move the file // Write the resulting CSS in the output folder
        .pipe(gulp.dest('app/libs/swiper/css'))
        //browser sync
        //.pipe(browserSync.reload({stream:true}));

    });

gulp.task('whhg_css', function () {
      return gulp
        // Find all `.css` files from the `stylesheets/` folder
        .src(SRC_WHHG_CSS)
        //rename the scss
        .pipe(rename({
                    suffix: ".min",
                }))
        //minify the css
        .pipe(minifyCss())
        //move the file // Write the resulting CSS in the output folder
        .pipe(gulp.dest('app/libs/webhostinghub-glyphs/css'))
        //browser sync
        //.pipe(browserSync.reload({stream:true}));

    });




/*=========================================================================    JS ========================*/


   gulp.task('bootstrap_js', function () {
      return gulp
        // Find all `.css` files from the `stylesheets/` folder
        .src(SRC_BOOTSTRAP_JS)
        //rename the scss
        .pipe(rename({
                    suffix: ".min",
                }))
        //minify the js
        .pipe(uglify())
        //move the file // Write the resulting CSS in the output folder
        .pipe(gulp.dest('app/libs/bootstrap/js'))
        //browser sync
        //.pipe(browserSync.reload({stream:true}));

    });

   gulp.task('jquery_js', function () {
      return gulp

        .src(SRC_JQUERY_JS)
        //rename the scss
        .pipe(rename({
                    suffix: ".min",
                }))
        //minify the js
        .pipe(uglify())
        //move the file // Write the resulting CSS in the output folder
        .pipe(gulp.dest('app/libs/jquery/js'))
        //browser sync
        //.pipe(browserSync.reload({stream:true}));

    });


   gulp.task('mixitup_js', function () {
      return gulp
        // Find all `.css` files from the `stylesheets/` folder
        .src(SRC_MIXITUP_JS)
        //rename the scss
        .pipe(rename({
                    suffix: ".min",
                }))
        //minify the js
        .pipe(uglify())
        //move the file // Write the resulting CSS in the output folder
        .pipe(gulp.dest('app/libs/mixitup/js'))
        //browser sync
        //.pipe(browserSync.reload({stream:true}));

    });

   gulp.task('swiper_js', function () {
      return gulp
        // Find all `.css` files from the `stylesheets/` folder
        .src(SRC_SWIPER_JS)
        //rename the scss
        .pipe(rename({
                    suffix: ".min",
                }))
        //minify the js
        .pipe(uglify())
        //move the file // Write the resulting CSS in the output folder
        .pipe(gulp.dest('app/libs/swiper/js'))
        //browser sync
        //.pipe(browserSync.reload({stream:true}));

    });





//  =============================================================================  WATCH FILES
    gulp.task('watch', ['browserSync', 'sass'], function (){
        //gulp.watch('app/index.php', ['useref']);
        gulp.watch(SRC_SCSS, ['sass']);
        gulp.watch(SRC_PHP, ['php']);
        gulp.watch(SRC_HTML, ['html']);

    });




//  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  RUN TASK FOR THE PROJECT
//  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// Default Task
gulp.task('default', ['bootstrap_css', 'fa_css', 'swiper_css', 'whhg_css', 'bootstrap_js', 'jquery_js', 'mixitup_js', 'swiper_js']);









//  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  CREATED FINAL PROJECT IN "dist" FOLDER
//  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


//  ========================================================================  IF NEED DELETE ALL FILES / FOLDER INSIDE FOLDER "dist"
    gulp.task('clean', function() {
        return gulp.src('dist/*', { read: false })
            .pipe(clean());
    });


//  ======================================================================  USEREF FONCTION : CONTACT & COMPRESS > CSS & JS
//  ==================================================================== (nb: the path to destination use the markup BUILD  & END BUILD in the html or php file)
    gulp.task('prod', function () {
        return gulp.src('app/*.php')
            .pipe(useref())
            .pipe(gulpIf('*.js', uglify()))
            .pipe(gulp.dest('dist'))
            .pipe(gulpIf('*.css', minifyCss()))
            .pipe(gulp.dest('dist'));
    });
