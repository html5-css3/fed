Projet d'integration web à partir d'une maquette (fichier jpg)

Dans le wiki du projet se trouve les fichiers de la maquette, les instructions avant projet et mes notes durant la réalisation du projet

Ce projet comprend :
- Dossier un dossier "app" concernant le developpement, un dossier "dist" concernant les fichiers finaux (pas encore créés) et un dossier "src" pour les ressources utilisée
- Les fichiers d'initialisation avec Git (versionning), Gulp (compilation fichier sass, live reload, concatenation fichier js et css) et installation de bootstrap (avec bower)
- Les fichiers de developpement du projet avec comme page principale : index.php, protfolio.php
- La feuille de style : style.css generé à partir de style.scss qui lui meme import des fichiers scss partiels
- L'integration les libraries annexe swiper.js et mixitup.js

INSTALLATION
- npm install (install gulp + plugins cf fichier package.json)
- bower install (install les dependces du projet, cf bower.json)
- gulp (run les tasks pour la creation des fichiers de src/bower_component à app/libs)
- gulp watch (lance le localhost , livereload et sass compilation)
