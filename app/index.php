<!--===============================================================   HEADER-->
<?php include("inc/inc_header.php"); ?>



<!--===============================================================   HERO -->
<section id=hero class="fixed-on-scroll row flex-items-xs-center" ><!--on positionne au milieu horizontalement-->
       <div class="flex-xs-middle"><!--on positionne au milieu verticalement-->
            <p>Awesome FED-2 </p>
            <a href="#"><span>Follow me</span></a><!--l'icone sous le paragraphe est ajoute via css _home.scss-->
       </div>
</section>

<!--===============================================================   NAV  -->
<?php include("inc/inc_nav.php"); ?>

<!--===============================================================   SERVICES  -->
<section id="services">
    <div class="container">
      <h1>Services</h1>
      <div class="row">
            <div class="col-xs"> <i class="icon-branch"></i>
                <h2>Versioning projet</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab non eius, magni cum sed dignissimos quas beatae minima quidem molestias impedit iste expedita sequi vitae commodi qui quod aliquid provident.</p>
            </div>
            <div class="col-xs"> <i class="icon-debug"></i>
                <h2>Debbuging system</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab non eius.</p>
            </div>
            <div class="col-xs"> <i class="icon-collabtive"></i>
                <h2>Collaborative work</h2>
                <p>Ab non eius, magni cum sed dignissimos quas beatae minima quidem molestias impedit iste expedita sequi vitae commodi qui quod aliquid provident.</p>
            </div>
            <div class="col-xs"> <i class="icon-cloudsync"></i>
                <h2>Cloud synchronization</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab non eius, magni cum sed dignissimos quas beatae minima quidem molestias impedit iste expedita .</p>
            </div>
      </div>
    </div>
</section>

<!--===============================================================   PORTFOLIO -->
<section id="portfolio">
    <div class="container">
        <h1>Portfolio</h1>

        <!--ici la 1er div permet d'appliquer le :hover... -->
        <div  class="overlay first-img" data-toggle="modal" data-target="#myModal">
         <!--  ... et la 2nd layer overlay avec :before, pas reussi à faire fonctionner le :before directement sur la balise img...-->
          <img class="img-fluid max-width: 100%;" src="images/portfolio/monkey.jpg">
        </div>

        <div class="overlay"  data-toggle="modal" data-target="#myModal">
            <img class="img-fluid max-width: 100%;" src="images/portfolio/monkey.jpg">
        </div>

        <div  class="overlay" data-toggle="modal" data-target="#myModal">
            <img class="img-fluid max-width: 100%;" src="images/portfolio/monkey.jpg">
        </div>

       <div  class="overlay" data-toggle="modal" data-target="#myModal">
            <img class="img-fluid max-width: 100%;" src="images/portfolio/monkey.jpg">
        </div>

        <div class="arrow-next">
                <a href="portfolio.php"><i class=" icon-circle-arrow-right"></i></a>
        </div>
    </div>



    <!--Ci dessous la fenetre modal qui est appele, pour le moment toutes les vignettes appel la meme fenetre, a modifier bien entendu-->

    <!------------------- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Smoking <Monkey></Monkey></h4>
          </div>
          <div class="modal-body">
             <div><img class="img-fluid max-width: 100%;" src="images/portfolio/monkey.jpg"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>

</section>


<!-- ==============================================================   VIDEO-->
<section id="video">
  <div class="container">
         <h1>Trailer video</h1>
      <div class="row">

        <div class="col-xs-8">


         <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam rerum modi, obcaecati in reprehenderit nobis commodi officia. Quas sequi dolorem culpa, repellat aperiam a, dolor fugit laudantium sapiente ratione ad. <br><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus minus, reprehenderit officiis temporibus maxime magni ut dolorem aspernatur a necessitatibus nihil nemo ipsam voluptatem, pariatur, eaque voluptate iusto veniam cumque.</p>
        </div>
        <div class="col-xs-4">
            <iframe  width="100%" min-height="500" height="auto" src="https://www.youtube.com/embed/iyTTX6Wlf1Y" frameborder="0" allowfullscreen></iframe>
        </div>

      </div>
    </div>
</section>



<!--==================================================================   RESEAUX SOCIAUX-->

<section id="reseau">
    <div class="container">
        <row>
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#profile" role="tab" data-toggle="tab"><h1>Facebook</h1></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#buzz" role="tab" data-toggle="tab"><h1>Twitter</h1></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#references" role="tab" data-toggle="tab"><h1>Linkedin</h1></a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="profile">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat iusto ea rerum odio eum necessitatibus illo hic laborum. Consectetur adipisci fugit, accusantium neque sunt laudantium delectus corrupti. Porro, animi, unde.</div>
                <div role="tabpanel" class="tab-pane fade" id="buzz">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, voluptate! Molestias dignissimos temporibus, aliquam totam fugiat aspernatur ex iusto repudiandae laudantium enim. Id, nulla. Illum architecto culpa officiis, repellendus doloremque. ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, officia. Eveniet accusantium fugit totam sed modi laudantium ab voluptates dolorem neque ipsum vel voluptatibus, saepe, sint facilis tenetur at ut.</div>
                <div role="tabpanel" class="tab-pane fade" id="references">etur adipisicing elit. Fugiat et iure esse explicabo maxime quibusdam ab, dolore assumenda. Eius libero assumenda in atque iure provident, delectus similique alias laboriosam iste?</div>
            </div>

        </row>
    </div>
</section>




<!--==================================================================   BIO   -->
<section id="bio">
 <div class="container">
 <h1>Bio let's talk about me</h1>
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio deserunt repudiandae laborum, est? A sequi cumque modi atque ipsam, libero amet mollitia! Reprehenderit reiciendis, perferendis rerum quis sed totam cupiditate?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt error voluptate, dolor fugit esse numquam laborum sint officia aspernatur deleniti quibusdam ex nulla obcaecati inventore quasi reiciendis tenetur. Laborum, deserunt?</p> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio deserunt repudiandae laborum, est? A sequi cumque modi atque ipsam, libero amet mollitia! Reprehenderit reiciendis, perferendis rerum quis sed totam cupiditate?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt error voluptate, dolor fugit esse numquam laborum sint officia aspernatur deleniti quibusdam ex nulla obcaecati inventore quasi reiciendis tenetur. Laborum, deserunt?</p>
    </div>
    <div class="col-xs-12 col-sm-6">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime veniam asperiores, saepe pariatur a suscipit laboriosam temporibus quo! Necessitatibus reiciendis nisi ea est quasi suscipit doloribus, consequatur quis nihil consectetur!</p>  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime veniam asperiores, saepe pariatur a suscipit laboriosam temporibus quo! Necessitatibus reiciendis nisi ea est quasi suscipit doloribus, consequatur quis nihil consectetur!</p>
      <div>
          <span><i class="icon-dribbble"></i></span>
          <span><i class="icon-facebook"></i></span>
          <span><i class="icon-googleplus"></i></span>
          <span><i class="icon-twitter"></i></span>
      </div>
    </div>
  </div>
</div>
</section>

<!--==================================================================   PARTENAIRES   -->
<section id="partenaires">
    <div class="container">

        <div class="row  row flex-items-xs-around">
            <div class="logo-partenaires flex-items-sm col-xs-12 col-sm-6 col-lg-4 col-xl-2">
              <img src="images/partenaires/logojpg.jpg">
            </div>
            <div class="logo-partenaires flex-items-sm col-xs-12 col-sm-6 col-lg-4 col-xl-2">
               <img src="images/partenaires/logojpg.jpg">
            </div>
            <div class="logo-partenaires flex-items-sm col-xs-12 col-sm-6 col-lg-4 col-xl-2">
               <img src="images/partenaires/logojpg.jpg">
            </div>
            <div class="logo-partenaires flex-items-sm  col-xs-12 col-sm-6 col-lg-4 col-xl-2">
              <img src="images/partenaires/logojpg.jpg">
            </div>
            <div class="logo-partenaires flex-items-sm col-xs-12 col-sm-6 col-lg-4 col-xl-2">
             <img src="images/partenaires/logojpg.jpg">
            </div>
        </div>
    </div>
</section>


<!--==================================================================   CONTACT   -->

<?php include("inc/inc_contact.php"); ?>


<!--==================================================================   FOOTER   -->
<?php include("inc/inc_footer.php"); ?>

