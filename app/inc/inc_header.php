<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Projet Front End Dev">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript, php, gulp, git, gitlab, bootstrap, sass">
    <meta name="author" content="26b.Fr">
    <meta name="robots" content="all" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">



<!--build:css css/styles.min.css-->
    <link href="libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="stylesheets/bootstrap-flex.min.css" rel="stylesheet" type="text/css">
    <link href="libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="libs/webhostinghub-glyphs/css/webhostinghub-glyphs.min.css" rel="stylesheet" type="text/css">
    <link href="libs/swiper/css/swiper.min.css" rel="stylesheet" type="text/css">


    <link href="stylesheets/styles.css" rel="stylesheet" type="text/css">



<!--endbuild-->
</head>
<body>

