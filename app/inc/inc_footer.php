 <section id="footer">
    <footer>
       <div class="container">



          <div class="row flex-items-xs-between">
            <div class="col-xs-4">
              <ul>
                  <li><a href="index.php">home</a></li>
                  <li><a href="portfolio.php">portfolio</a></li>
                  <li><a href="contact.php">contact</a></li>
              </ul>
            </div>
            <div class="col-xs-4">
                          <a class="navbar-brand " href="index.php">Awesome <span>Fed</span> #2</a>
            <div>Copyright 2015 / <?php echo date("Y"); ?></div>
            </div>
          </div>


        </div>
    </footer>
</section>

    <!-- build:js js/scripts.min.js -->
    <script type="text/javascript" src="libs/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="libs/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="libs/swiper/js/swiper.min.js"></script>
    <script type="text/javascript" src="libs/mixitup/js/mixitup.min.js"></script>



    <script type="text/javascript" src="scripts/slider.js"></script>
    <script type="text/javascript" src="scripts/nav.js"></script>
    <script type="text/javascript" src="scripts/mixitup.js"></script>

    <!-- endbuild -->


</body>
</html>
