<div id="mixitup">
    <div class="container">
        <div class="row flex-items-xs-between">
            <div class="col-xs-4">
                <h1>Portfolio</h1>
            </div>


            <div class="col-xs-4">
                <div class="controls">
                    <button type="button" class="control" data-filter="all">All</button>
                    <button type="button" class="control" data-toggle=".smoke">Smoke</button>
                    <button type="button" class="control" data-toggle=".roses">Roses</button>
                    <button type="button" class="control" data-toggle=".other">Other</button>
                </div>
            </div>
        </div>



        <div class="portfolio-container">
            <div class="overlay mix green"><img src="images/portfolio_galerie/p4.jpg" alt""></div>
            <div class="overlay mix other"><img src="images/portfolio_galerie/p1.jpg" alt=""></div>
            <div class="overlay mix roses"><img src="images/portfolio_galerie/p2.jpg" alt=""></div>
            <div class="overlay mix smoke"><img src="images/portfolio_galerie/p3.jpg" alt=""></div>
            <div class="overlay mix other"><img src="images/portfolio_galerie/p4.jpg" alt""></div>
            <div class="overlay mix other"><img src="images/portfolio_galerie/p5.jpg" alt=""></div>
            <div class="overlay mix smoke"><img src="images/portfolio_galerie/p6.jpg" alt=""></div>
            <div class="overlay mix other"><img src="images/portfolio_galerie/p4.jpg" alt=""></div>
            <div class="overlay mix other"><img src="images/portfolio_galerie/p4.jpg" alt=""></div>
            <div class="overlay mix other"><img src="images/portfolio_galerie/p1.jpg" alt=""></div>
            <div class="overlay mix roses"><img src="images/portfolio_galerie/p2.jpg" alt=""></div>
            <div class="overlay mix smoke"><img src="images/portfolio_galerie/p3.jpg" alt=""></div>
            <div class="overlay mix other"><img src="images/portfolio_galerie/p4.jpg" alt""></div>
            <div class="overlay mix other"><img src="images/portfolio_galerie/p5.jpg" alt=""></div>
            <div class="overlay mix smoke"><img src="images/portfolio_galerie/p6.jpg" alt=""></div>



        </div>

    </div>
</div>
