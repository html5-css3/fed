<section id="slider" class="fixed-on-scroll">
        <!-- Swiper -->
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="s1 swiper-slide"></div>
            <div class="s2 swiper-slide"></div>
            <div class="s3 swiper-slide"></div>

        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <!-- Add Pagination -->
        <!--<div class="swiper-pagination"></div>-->
    </div>
</section>
