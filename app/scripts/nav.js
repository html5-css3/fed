$("#navbar a").on("click", function(){
   $("#navbar").find(".active").removeClass("active");
   $(this).parent().addClass("active");
});



// lorsque que l"on scroll, si la hauteur du scroll est superieur au conteneur #hero (le conteneur se trouvant au dessus de la navbar) on ajoute la class nav-bar-fixed-top pour fixer la nav sinon on l'enleve...
document.onscroll = function() {
    if( $(window).scrollTop() > $('.fixed-on-scroll').height() ) {
        $('#navbar').removeClass('navbar-static-top').addClass('navbar-fixed-top');
    }
    else {
        $('#navbar').removeClass('navbar-fixed-top').addClass('navbar-static-top');
    }
};


